package com.atguigu.gmallpublisher.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface SugarMapper {

    @Select("select sum(order_amount) from dws_trade_province_order_window where toYYYYMMDD(stt)=#{date}")
    BigDecimal selectGmv(int date);

    /*
    *   ┌─trademark_name─┬─────────────────────────gmv─┬─ct─┐
        │ 苹果           │ 978537.00000000000000000000 │  8 │
        │ 华为           │ 640390.18000000000000000000 │  9 │
        │ 三星           │ 486554.00000000000000000000 │ 12 │
        │ TCL            │ 380258.00000000000000000000 │  4 │
        │ 小米           │ 133956.00000000000000000000 │  3 │
        └────────────────┴─────────────────────────────┴────┘
    * */
    @Select("select trademark_name,sum(order_amount) gmv,count(*) ct from dws_trade_sku_order_window where toYYYYMMDD(stt)=#{date} group by trademark_name order by gmv desc limit #{limit}")
    List<Map> selectGmvByTm(@Param("date") int date, @Param("limit") int limit);


}
