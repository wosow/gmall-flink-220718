package com.atguigu.gmallpublisher.service.impl;

import com.atguigu.gmallpublisher.mapper.SugarMapper;
import com.atguigu.gmallpublisher.service.SugarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SugarServiceImpl implements SugarService {

    @Autowired
    private SugarMapper sugarMapper;

    @Override
    public BigDecimal getGmv(int date) {
        return sugarMapper.selectGmv(date);
    }

    @Override
    public Map getGmvByTm(int date, int limit) {

        //查询数据
            /*
    *   ┌─trademark_name─┬─────────────────────────gmv─┬─ct─┐
        │ 苹果           │ 978537.00000000000000000000 │  8 │
        │ 华为           │ 640390.18000000000000000000 │  9 │
        │ 三星           │ 486554.00000000000000000000 │ 12 │
        │ TCL            │ 380258.00000000000000000000 │  4 │
        │ 小米           │ 133956.00000000000000000000 │  3 │
        └────────────────┴─────────────────────────────┴────┘
    * */
        List<Map> mapList = sugarMapper.selectGmvByTm(date, limit);

        //创建Map用于存放结果数据
        HashMap<String, BigDecimal> result = new HashMap<>();

        for (Map map : mapList) {
            //map:[(trademark_name->苹果),(gmv->978537),(ct->8)]
            result.put((String) map.get("trademark_name"), (BigDecimal) map.get("gmv"));
        }

        return result;
    }
}
