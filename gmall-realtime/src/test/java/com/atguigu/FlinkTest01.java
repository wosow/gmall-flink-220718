package com.atguigu;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class FlinkTest01 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);

        DataStream<String> socketTextStream = env.socketTextStream("hadoop102", 9999);

        SingleOutputStreamOperator<String> mapDS = socketTextStream.map(new MapFunction<String, String>() {
            @Override
            public String map(String value) throws Exception {
                return value;
            }
        });

        mapDS.print("Map>>>>>>>>>>>");

        mapDS.global().print("global>>>>>>>").setParallelism(4);
        mapDS.broadcast().print("broadcast>>>>>>").setParallelism(4);
        //mapDS.forward().print("forward>>>>>>>>").setParallelism(4);

        env.execute();
    }

}
