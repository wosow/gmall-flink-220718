package com.atguigu.app.func;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.utils.DruidDSUtil;
import com.atguigu.utils.ThreadPoolUtil;
import lombok.SneakyThrows;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;

import java.util.Collections;
import java.util.concurrent.ThreadPoolExecutor;

public abstract class DimAsyncJoinFunction<T> extends RichAsyncFunction<T, T> implements JoinFunction<T> {

    private String tableName;
    private DruidDataSource druidDataSource;
    private ThreadPoolExecutor threadPoolExecutor;

    public DimAsyncJoinFunction(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        druidDataSource = DruidDSUtil.getDruidDataSource();
        threadPoolExecutor = ThreadPoolUtil.getThreadPoolExecutor();
    }

    @Override
    public void asyncInvoke(T input, ResultFuture<T> resultFuture) throws Exception {

        threadPoolExecutor.execute(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                DruidPooledConnection connection = druidDataSource.getConnection();

                //查询维表数据
                String key = getKey(input);
                JSONObject dimInfo = DimUtil.getDimInfo(connection, tableName, key);

                //补充至数据中
                if (dimInfo != null) {
                    join(input, dimInfo);
                }

                //写回流
                resultFuture.complete(Collections.singletonList(input));

                //回收Phoenix连接
                connection.close();
            }
        });
    }

    @Override
    public void timeout(T input, ResultFuture<T> resultFuture) throws Exception {
        System.out.println("TimeOut>>>>>>>>>>>" + input);
    }
}
