package com.atguigu.utils;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.GmallConfig;
import com.google.common.base.CaseFormat;
import org.apache.commons.beanutils.BeanUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * t 主键:id
 * select count(*) from t;            单行单列
 * select * from t where id='1001';   单行多列
 * select id from t;                  多行单列
 * select * from t;                   多行多列
 * id name sex
 * 11 zs   male
 * 12 ls   female
 */
public class JdbcUtil {

    public static <T> List<T> queryList(Connection connection, String querySql, Class<T> clz, boolean underScoreToCamel) throws Exception {

        //创建集合用于存放结果数据
        ArrayList<T> list = new ArrayList<>();

        //编译SQL
        PreparedStatement preparedStatement = connection.prepareStatement(querySql);

        //执行查询
        ResultSet resultSet = preparedStatement.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        //遍历查询结果,将每行数据转换为T对象放入集合
        while (resultSet.next()) {
            //构建T对象
            T t = clz.newInstance();

            //遍历列
            for (int i = 0; i < columnCount; i++) {
                //获取列名和数据
                String columnName = metaData.getColumnName(i + 1);
//                resultSet.getObject(i + 1);
                Object value = resultSet.getObject(columnName);

                if (underScoreToCamel) {
                    columnName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, columnName.toLowerCase());
                }

                BeanUtils.setProperty(t, columnName, value);
            }

            //将封装好的T对象添加至集合
            list.add(t);
        }

        //释放资源
        resultSet.close();
        preparedStatement.close();

        //返回集合
        return list;
    }

    public static void main(String[] args) throws Exception {

        //Class.forName("");
//        Connection connection = DriverManager.getConnection("jdbc:mysql://hadoop102:3306/gmall-220718-config?user=root&password=000000&useUnicode=true&characterEncoding=utf8&serverTimeZone=Asia/Shanghai&useSSL=false");
        Connection connection = DriverManager.getConnection(GmallConfig.PHOENIX_SERVER);

        List<JSONObject> tableProcesses = queryList(connection,
                "select count(*) ct from GMALL220623_REALTIME.DIM_BASE_TRADEMARK",
                JSONObject.class,
                false);

        for (JSONObject tableProcess : tableProcesses) {
            System.out.println(tableProcess);
        }

        connection.close();

    }

}
